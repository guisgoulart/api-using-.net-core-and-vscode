﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models;
using BLL;

namespace testeAPI.Controllers
{
   [Route("api/[controller]")]

    //Para acessar no Localhost, a rota é: http://localhost:PORTADEBUILD/api/pegaCliente/Get
    //PORTADEBUILD ao dar dotnet run. Geralmente é 5000 para http e 5001 para https

   public class pegaCliente : Controller
   {
       // GET api/values
       [HttpGet("{id}")]
       public List<cliente> Get(){
           return new BCliente().RetornarClientes();
       }      
   }
}
