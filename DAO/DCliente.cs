
using System.Collections.Generic;
using System;
using Models;
using System.Data;
using System.Data.SqlClient;

namespace DAO
{

   public class DAOCliente{

       public List<cliente> RetornarCliente(){
           var connString = "SUA STRING DE CONEXAO";

           List<cliente> lstCliente = new List<cliente>();

           try{
               using (var conn = new SqlConnection(connString))
               {
                   conn.Open();
                   using (var cmd = new SqlCommand("SELECT idCliente, nome, email FROM clientes", conn))
                   using (var reader = cmd.ExecuteReader())
                   while (reader.Read()){
                       cliente Cliente = new cliente();
                       Cliente.id = (int)reader["idCliente"];
                       Cliente.nome = (string)reader["nome"];
                       Cliente.email = reader.GetString(2);
                       lstCliente.Add(Cliente);
                   }
                   conn.Close();
               }
           }catch(Exception ex){
               string teste = ex.Message;
           }      
       
           return lstCliente;
       }
   }
}